/*
 * FLP 2016/2017
 * Rubik's Cube with BFS
 * Daniel Haris (xharis00)
 */

/**********************
 * Dynamic predicates *
 **********************/

:- dynamic open/2, closed/2.

/*********
 * Facts *
 *********/

/* List of available colors of the RC */
available_colors(['1','2','3','4','5','6']).

/* Finished RC */
finished_cube([
	['1','1','1','1','1','1','1','1','1'],
	['2','2','2','2','2','2','2','2','2'],
	['3','3','3','3','3','3','3','3','3'],
	['4','4','4','4','4','4','4','4','4'],
	['5','5','5','5','5','5','5','5','5'],
	['6','6','6','6','6','6','6','6','6']
]).

/**************************************
 * Predicates for processing input RC *
 **************************************/

/* Reads line from stdin, terminates on LF or EOF */
read_line(L,C) :- get_char(C), (isEOFEOL(C), L = [], !; read_line(LL,_), [C|LL] = L).

/* Tests if character is EOF or LF */
isEOFEOL(C) :- C == end_of_file; (char_code(C,Code), Code==10).

/* Reads lines from stdin and terminates on EOF or EOL */
read_lines(Ls) :- read_line(L,C), (C == end_of_file, Ls = []; read_lines(LLs), Ls = [L|LLs]).

/* splits line into sublists */
split_line([],[[]]) :- !.
split_line([' '|T], [[]|S1]) :- !, split_line(T,S1).
split_line([32|T], [[]|S1]) :- !, split_line(T,S1).
split_line([H|T], [[H|G]|S1]) :- split_line(T,[G|S1]).

/* input is list of lines */
split_lines([],[]).
split_lines([L|Ls],[H|T]) :- split_lines(Ls,T), split_line(L,H).

/***********************************
 * Predicates for validation of RC *
 ***********************************/

/* The given list contains only unique values */
unique_list(L) :- \+ (select(X,L,R), memberchk(X,R)).

/* The given color is valid */
is_valid_color(Color) :- available_colors(ListOfColors), member(Color, ListOfColors).

/* The given list contains only valid colors */
is_valid_list_of_colors([]) :- !.
is_valid_list_of_colors([Color|T]) :- is_valid_color(Color), is_valid_list_of_colors(T).

/* the 3 given pieces adds up a valid corner in the cube */
is_valid_corner([X,Y,Z]) :- X \= Y, Y \= Z, X \= Z.

/* the 2 given pieces adds up a valid edge in the cube */
is_valid_edge([X,Y]) :- X \= Y.

/* whether cube has valid center pieces */
cube_has_valid_centers(ListOfCenters) :- unique_list(ListOfCenters).

/* whether cube has valid edges */
cube_has_valid_edges([]) :- !.
cube_has_valid_edges([Edge|T]) :- is_valid_edge(Edge), cube_has_valid_edges(T).

/* whether cube has valid corners */
cube_has_valid_corners([]) :- !.
cube_has_valid_corners([Corner|T]) :- is_valid_corner(Corner), cube_has_valid_corners(T).

/* whether cube has valid corners */
cube_has_valid_colors([]) :- !.
cube_has_valid_colors([Wall|T]) :- is_valid_list_of_colors(Wall), cube_has_valid_colors(T).

/* the main validation predicate */
is_valid_cube(
	[
		[W1,W2,W3,W4,W5,W6,W7,W8,W9],
		[O1,O2,O3,O4,O5,O6,O7,O8,O9],
		[Y1,Y2,Y3,Y4,Y5,Y6,Y7,Y8,Y9],
		[R1,R2,R3,R4,R5,R6,R7,R8,R9],
		[G1,G2,G3,G4,G5,G6,G7,G8,G9],
		[B1,B2,B3,B4,B5,B6,B7,B8,B9]
	]
) :-
	cube_has_valid_centers([W5,O5,Y5,R5,G5,B5]),
	cube_has_valid_edges([
		[W2,G8],[W6,O4],[W8,B8],[W4,R6],
		[O2,G6],[O8,B4],[Y2,G2],[Y6,R4],
		[Y8,B2],[Y4,O6],[R8,B6],[R2,G4]
	]),
	cube_has_valid_corners([
		[W1,R3,G7],[W3,O1,G9],[W7,R9,B1],[W9,O7,B3],
		[Y1,O3,G3],[Y3,R1,G1],[Y7,O9,B9],[Y9,R7,B7]
	]),
	cube_has_valid_colors([
		[W1,W2,W3,W4,W5,W6,W7,W8,W9],
		[O1,O2,O3,O4,O5,O6,O7,O8,O9],
		[Y1,Y2,Y3,Y4,Y5,Y6,Y7,Y8,Y9],
		[R1,R2,R3,R4,R5,R6,R7,R8,R9],
		[G1,G2,G3,G4,G5,G6,G7,G8,G9],
		[B1,B2,B3,B4,B5,B6,B7,B8,B9]
	]).

/*************************************************************
 * Cube moves                                                *
 * inspired from https://ruwix.com/the-rubiks-cube/notation/ *
 * all of the listed moves are clockwise moves, predicate    *
 * move/3 implements also counterclockwise moves             *
 *************************************************************/

/* left */
mov(
	l, [
		[W1,W2,W3,W4,W5,W6,W7,W8,W9],
		[O1,O2,O3,O4,O5,O6,O7,O8,O9],
		[Y1,Y2,Y3,Y4,Y5,Y6,Y7,Y8,Y9],
		[R1,R2,R3,R4,R5,R6,R7,R8,R9],
		[G1,G2,G3,G4,G5,G6,G7,G8,G9],
		[B1,B2,B3,B4,B5,B6,B7,B8,B9]
	], [
		[G1,W2,W3,G4,W5,W6,G7,W8,W9],
		[O1,O2,O3,O4,O5,O6,O7,O8,O9],
		[Y1,Y2,B7,Y4,Y5,B4,Y7,Y8,B1],
		[R7,R4,R1,R8,R5,R2,R9,R6,R3],
		[Y9,G2,G3,Y6,G5,G6,Y3,G8,G9],
		[W1,B2,B3,W4,B5,B6,W7,B8,B9]
	]
).
/* right */
mov(
	r,
	[
		[W1,W2,W3,W4,W5,W6,W7,W8,W9],
		[O1,O2,O3,O4,O5,O6,O7,O8,O9],
		[Y1,Y2,Y3,Y4,Y5,Y6,Y7,Y8,Y9],
		[R1,R2,R3,R4,R5,R6,R7,R8,R9],
		[G1,G2,G3,G4,G5,G6,G7,G8,G9],
		[B1,B2,B3,B4,B5,B6,B7,B8,B9]
	], [
		[W1,W2,B3,W4,W5,B6,W7,W8,B9],
		[O7,O4,O1,O8,O5,O2,O9,O6,O3],
		[G9,Y2,Y3,G6,Y5,Y6,G3,Y8,Y9],
		[R1,R2,R3,R4,R5,R6,R7,R8,R9],
		[G1,G2,W3,G4,G5,W6,G7,G8,W9],
		[B1,B2,Y7,B4,B5,Y4,B7,B8,Y1]
	]
).
/* up */
mov(
	u,
	[
		[W1,W2,W3,W4,W5,W6,W7,W8,W9],
		[O1,O2,O3,O4,O5,O6,O7,O8,O9],
		[Y1,Y2,Y3,Y4,Y5,Y6,Y7,Y8,Y9],
		[R1,R2,R3,R4,R5,R6,R7,R8,R9],
		[G1,G2,G3,G4,G5,G6,G7,G8,G9],
		[B1,B2,B3,B4,B5,B6,B7,B8,B9]
	], [
		[O1,O2,O3,W4,W5,W6,W7,W8,W9],
		[Y1,Y2,Y3,O4,O5,O6,O7,O8,O9],
		[R1,R2,R3,Y4,Y5,Y6,Y7,Y8,Y9],
		[W1,W2,W3,R4,R5,R6,R7,R8,R9],
		[G7,G4,G1,G8,G5,G2,G9,G6,G3],
		[B1,B2,B3,B4,B5,B6,B7,B8,B9]
	]
).
/* down */
mov(
	d,
	[
		[W1,W2,W3,W4,W5,W6,W7,W8,W9],
		[O1,O2,O3,O4,O5,O6,O7,O8,O9],
		[Y1,Y2,Y3,Y4,Y5,Y6,Y7,Y8,Y9],
		[R1,R2,R3,R4,R5,R6,R7,R8,R9],
		[G1,G2,G3,G4,G5,G6,G7,G8,G9],
		[B1,B2,B3,B4,B5,B6,B7,B8,B9]
	], [
		[W1,W2,W3,W4,W5,W6,R7,R8,R9],
		[O1,O2,O3,O4,O5,O6,W7,W8,W9],
		[Y1,Y2,Y3,Y4,Y5,Y6,O7,O8,O9],
		[R1,R2,R3,R4,R5,R6,Y7,Y8,Y9],
		[G1,G2,G3,G4,G5,G6,G7,G8,G9],
		[B7,B4,B1,B8,B5,B2,B9,B6,B3]
	]
).
/* front */
mov(
	f,
	[
		[W1,W2,W3,W4,W5,W6,W7,W8,W9],
		[O1,O2,O3,O4,O5,O6,O7,O8,O9],
		[Y1,Y2,Y3,Y4,Y5,Y6,Y7,Y8,Y9],
		[R1,R2,R3,R4,R5,R6,R7,R8,R9],
		[G1,G2,G3,G4,G5,G6,G7,G8,G9],
		[B1,B2,B3,B4,B5,B6,B7,B8,B9]
	], [
		[W7,W4,W1,W8,W5,W2,W9,W6,W3],
		[G7,O2,O3,G8,O5,O6,G9,O8,O9],
		[Y1,Y2,Y3,Y4,Y5,Y6,Y7,Y8,Y9],
		[R1,R2,B1,R4,R5,B2,R7,R8,B3],
		[G1,G2,G3,G4,G5,G6,R9,R6,R3],
		[O7,O4,O1,B4,B5,B6,B7,B8,B9]
	]
).
/* back */
mov(
	b,
	[
		[W1,W2,W3,W4,W5,W6,W7,W8,W9],
		[O1,O2,O3,O4,O5,O6,O7,O8,O9],
		[Y1,Y2,Y3,Y4,Y5,Y6,Y7,Y8,Y9],
		[R1,R2,R3,R4,R5,R6,R7,R8,R9],
		[G1,G2,G3,G4,G5,G6,G7,G8,G9],
		[B1,B2,B3,B4,B5,B6,B7,B8,B9]
	], [
		[W1,W2,W3,W4,W5,W6,W7,W8,W9],
		[O1,O2,B9,O4,O5,B8,O7,O8,B7],
		[Y7,Y4,Y1,Y8,Y5,Y2,Y9,Y6,Y3],
		[G3,R2,R3,G2,R5,R6,G1,R8,R9],
		[O3,O6,O9,G4,G5,G6,G7,G8,G9],
		[B1,B2,B3,B4,B5,B6,R1,R4,R7]
	]
).
/* middle */
mov(
	m,
	[
		[W1,W2,W3,W4,W5,W6,W7,W8,W9],
		[O1,O2,O3,O4,O5,O6,O7,O8,O9],
		[Y1,Y2,Y3,Y4,Y5,Y6,Y7,Y8,Y9],
		[R1,R2,R3,R4,R5,R6,R7,R8,R9],
		[G1,G2,G3,G4,G5,G6,G7,G8,G9],
		[B1,B2,B3,B4,B5,B6,B7,B8,B9]
	], [
		[W1,G2,W3,W4,G5,W6,W7,G8,W9],
		[O1,O2,O3,O4,O5,O6,O7,O8,O9],
		[Y1,B8,Y3,Y4,B5,Y6,Y7,B2,Y9],
		[R1,R2,R3,R4,R5,R6,R7,R8,R9],
		[G1,Y8,G3,G4,Y5,G6,G7,Y2,G9],
		[B1,W2,B3,B4,W5,B6,B7,W8,B9]
	]
).
/* equator */
mov(
	e,
	[
		[W1,W2,W3,W4,W5,W6,W7,W8,W9],
		[O1,O2,O3,O4,O5,O6,O7,O8,O9],
		[Y1,Y2,Y3,Y4,Y5,Y6,Y7,Y8,Y9],
		[R1,R2,R3,R4,R5,R6,R7,R8,R9],
		[G1,G2,G3,G4,G5,G6,G7,G8,G9],
		[B1,B2,B3,B4,B5,B6,B7,B8,B9]
	], [
		[W1,W2,W3,R4,R5,R6,W7,W8,W9],
		[O1,O2,O3,W4,W5,W6,O7,O8,O9],
		[Y1,Y2,Y3,O4,O5,O6,Y7,Y8,Y9],
		[R1,R2,R3,Y4,Y5,Y6,R7,R8,R9],
		[G1,G2,G3,G4,G5,G6,G7,G8,G9],
		[B1,B2,B3,B4,B5,B6,B7,B8,B9]
	]
).
/* standing */
mov(
	s,
	[
		[W1,W2,W3,W4,W5,W6,W7,W8,W9],
		[O1,O2,O3,O4,O5,O6,O7,O8,O9],
		[Y1,Y2,Y3,Y4,Y5,Y6,Y7,Y8,Y9],
		[R1,R2,R3,R4,R5,R6,R7,R8,R9],
		[G1,G2,G3,G4,G5,G6,G7,G8,G9],
		[B1,B2,B3,B4,B5,B6,B7,B8,B9]
	], [
		[W1,W2,W3,W4,W5,W6,W7,W8,W9],
		[O1,G4,O3,O4,G5,O6,O7,G6,O9],
		[Y1,Y2,Y3,Y4,Y5,Y6,Y7,Y8,Y9],
		[R1,B4,R3,R4,B5,R6,R7,B6,R9],
		[G1,G2,G3,R8,R5,R2,G7,G8,G9],
		[B1,B2,B3,O8,O5,O2,B7,B8,B9]
	]
).

/* more abstract predicate which allows also counterclockwise (-) moves */
move(+M, OldState, NewState) :- mov(M, OldState, NewState). 
move(-M, OldState, NewState) :- mov(M, NewState, OldState).

/*************************************
 * Solving the RC with BFS algorithm *
 *************************************/

/* predicate prints out the solution path from input cube to finished cube */
print_solution([State,nil]) :- print_cube(State).
print_solution([State,Parent]):-
	closed(Parent,Grandparent),
	print_solution([Parent,Grandparent]),
	format('~n'), print_cube(State).

/**
 * main predicate for solving the cube
 * - uses dynamic predicates: open, closed 
*/
solve_cube(InputCube) :- assert(open(InputCube,nil)), bfs().

/**
 * predicate for BFS algorithm
 */
bfs() :-
	open(State,OldState),
	finished_cube(State),
	assertz(closed(State,OldState)),
	print_solution([State,OldState]).

bfs() :-
	retract(open(State,OldState)),
	expand_bfs(State,Children),
	assertz(closed(State,OldState)),
	append_children_to_open(Children),
	append_children_to_closed(Children),
	bfs().

/* - helper predicates, which adds to queues only those children,
 *   which haven't been visited in the past yet
 * - children which are present in queues are simply skipped
 */
append_children_to_open([]).
append_children_to_open([[Child,Parent]|RestOfChilren]) :-
	open(Child,Parent),
	append_children_to_open(RestOfChilren).
append_children_to_open([[Child,Parent]|RestOfChilren]) :-
	assertz(open(Child,Parent)),
	append_children_to_open(RestOfChilren).

append_children_to_closed([]).
append_children_to_closed([[Child,Parent]|RestOfChilren]) :-
	closed(Child,Parent),
	append_children_to_closed(RestOfChilren).
append_children_to_closed([[Child,Parent]|RestOfChilren]) :-
	assertz(closed(Child,Parent)),
	append_children_to_closed(RestOfChilren).

/* expansion predicate, which generates new states by defined cube moves */
expand_bfs(
	State, 
	[
		[Lplus,State], [Lminus,State],
		[Rplus,State], [Rminus,State],
		[Uplus,State], [Uminus,State],
		[Dplus,State], [Dminus,State],
		[Fplus,State], [Fminus,State],
		[Bplus,State], [Bminus,State],
		[Mplus,State], [Mminus,State],
		[Eplus,State], [Eminus,State],
		[Splus,State], [Sminus,State]
	]
) :-
	move(+l,State,Lplus), move(-l,State,Lminus),
	move(+r,State,Rplus), move(-r,State,Rminus),
	move(+u,State,Uplus), move(-u,State,Uminus),
	move(+d,State,Dplus), move(-d,State,Dminus),
	move(+f,State,Fplus), move(-f,State,Fminus),
	move(+b,State,Bplus), move(-b,State,Bminus),
	move(+m,State,Mplus), move(-m,State,Mminus),
	move(+e,State,Eplus), move(-e,State,Eminus),
	move(+s,State,Splus), move(-s,State,Sminus).

/* transform cube into internal representation */
transform_cube(
	[
		[[G1,G2,G3]],
		[[G4,G5,G6]],
		[[G7,G8,G9]],
		[[W1,W2,W3],[O1,O2,O3],[Y1,Y2,Y3],[R1,R2,R3]],
		[[W4,W5,W6],[O4,O5,O6],[Y4,Y5,Y6],[R4,R5,R6]],
		[[W7,W8,W9],[O7,O8,O9],[Y7,Y8,Y9],[R7,R8,R9]],
		[[B1,B2,B3]],
		[[B4,B5,B6]],
		[[B7,B8,B9]]
	], [
		[W1,W2,W3,W4,W5,W6,W7,W8,W9],
		[O1,O2,O3,O4,O5,O6,O7,O8,O9],
		[Y1,Y2,Y3,Y4,Y5,Y6,Y7,Y8,Y9],
		[R1,R2,R3,R4,R5,R6,R7,R8,R9],
		[G1,G2,G3,G4,G5,G6,G7,G8,G9],
		[B1,B2,B3,B4,B5,B6,B7,B8,B9]
	]
).

/* print out cube to stdout */
print_cube(
	[
		[W1,W2,W3,W4,W5,W6,W7,W8,W9],
		[O1,O2,O3,O4,O5,O6,O7,O8,O9],
		[Y1,Y2,Y3,Y4,Y5,Y6,Y7,Y8,Y9],
		[R1,R2,R3,R4,R5,R6,R7,R8,R9],
		[G1,G2,G3,G4,G5,G6,G7,G8,G9],
		[B1,B2,B3,B4,B5,B6,B7,B8,B9]
	]
) :- 
	format('~w~w~w~n', [G1,G2,G3]),
	format('~w~w~w~n', [G4,G5,G6]),
	format('~w~w~w~n', [G7,G8,G9]),
	format('~w~w~w ~w~w~w ~w~w~w ~w~w~w~n', [W1,W2,W3, O1,O2,O3, Y1,Y2,Y3, R1,R2,R3]),
	format('~w~w~w ~w~w~w ~w~w~w ~w~w~w~n', [W4,W5,W6, O4,O5,O6, Y4,Y5,Y6, R4,R5,R6]),
	format('~w~w~w ~w~w~w ~w~w~w ~w~w~w~n', [W7,W8,W9, O7,O8,O9, Y7,Y8,Y9, R7,R8,R9]),
	format('~w~w~w~n', [B1,B2,B3]),
	format('~w~w~w~n', [B4,B5,B6]),
	format('~w~w~w~n', [B7,B8,B9])
.

/* main predicate */
start :-
	prompt(_, ''),
	read_lines(LL),
	split_lines(LL,S),
	transform_cube(S,Y),
	is_valid_cube(Y),
	solve_cube(Y),
	halt.
