CC=swipl
FLAGS=-q -g
FILE=rubic.pl
EXE=flp17-log

all:
	$(CC) $(FLAGS) start -o $(EXE) -c $(FILE)
test:
	./$(EXE) < tests/test0_in > outputs/test0_out
	./$(EXE) < tests/test1_in > outputs/test1_out
	./$(EXE) < tests/test2_in > outputs/test2_out
	./$(EXE) < tests/test3_in > outputs/test3_out
	./$(EXE) < tests/test4_in > outputs/test4_out
	./$(EXE) < tests/test5_in > outputs/test5_out
clean:
	rm -f $(EXE)
	rm -f outputs/*

